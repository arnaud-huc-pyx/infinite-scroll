/**
 *  Infinite Scroll
 *
 *  a jQuery/Zepto plugin for an infinite scroll. To inite it, just pass the .InfiniteScroll() to the content who
 *
 *  will be scrolled infinitely
 *
 *  Here is the list of options you can passed into the .InfiniteScroll().
 *
 *  {
 *   max : int,                 | set how many time the scroll infinite will automaticaly load content
 *   last : int,                | set how many pages max you want to load. If not set, last = max
 *   ajaxUrl : string,          | the url of the page you want to load to
 *   idButtonPlus : string,     | the id of the button to load more pages once the auto-loaded pages stop
 *  }
 *
 */
(function($) {
    'use strict';

    var pluginName = 'InfiniteScroll',
        plugNumb = 0;

    $[pluginName] = (function(w) {

        var $w = $(w);

        /*
         * Plugin Constructor. This function build the basic object for the plugin
         * @param (object) elt - The jQuery or Zepto DOM element
         * @param (object) opts - A list of options for the plugin
         */
        $[pluginName] = function(elt, options) {
            var defaults = {
                    max: false, // If not set, the plugin dont start
                    last: false // If not set, the plugin dont start
                },
                i = 2,
                flag = true,
                e = '', //error message if there is one
                opts = (typeof options !== 'string') ? $.extend({}, defaults, options) : options;
            // constructor body
            this._name = pluginName;
            this.plugInst = 'scroll.'+pluginName+plugNumb;
            plugNumb++;
            this.elt = elt;
            this.loading = '<div class="loader-wrapper"><span class="scroll-loader"></span></div>';
            this.ajaxUrl = opts.ajaxUrl;
            this.max = opts.max ? opts.max : defaults.max;
            this.last = opts.last > this.max && opts.last ? opts.last : (opts.max ? opts.max : e +="Please use the last and/or max options"); // if last is not define, last = max
            this.clickTapEvent = 'onTap' in w ? 'tap' : 'click';
            this.clickElement = $(opts.idButtonPlus);
            if(typeof(this.last) === 'string'){this.clickElement.hide(); };
            this.response = [];
            this.status= [];
            /*
             * Ajax's method for the Plugin
             * @param {object} args - Plugin object who contain each data set into it
             */
            this.ajaxMethod = function(args) {
                flag = false;
                var plugin = args;
                var getParam = i === 0 ? {} : {
                    p : i
                }
                $.ajax({
                    type: 'GET',
                    url: plugin.ajaxUrl,
                    data: getParam,
                    contentType : false,
                    dataType: 'html',
                    success: function(response, status) {
                        plugin.elt.append(response);
                        plugin.response.push(response);
                        i++;
                    },
                    beforeSend: function() {
                        plugin.elt.append(plugin.loading);
                    },
                    complete: function(xhr, status) {
                        $('.loader').remove();
                        flag = true;
                    }
                    ,error : function (xhr, errorType){
                        console.error(errorType, xhr['status']);
                    }
                });
                if (i == plugin.last) {
                    plugin.destroy()
                };
            };
            /*
             *  Method to launch the process to infinite (and beyond)
             *  @param {object} args
             */
            this.create = function(args, opts) {
                //If an error occured, stop the create action
                if(e !== ''){console.error(e);return;}


                var plugin = args,
                    options = opts;

                if(options !== undefined){
                    for(var values in options){
                        if(values === "last"){
                            if(options[values] >= plugin.max){
                                plugin.last = options[values];
                            }else if(options[values] < plugin.max){
                                plugin.max = plugin.last = options[values];
                            }
                        }else if(values === "max"){
                            if(options[values] <= plugin.last){
                                plugin.max = options[values];
                            }else if (options[values] > plugin.last){
                                plugin.max = plugin.last = options[values];
                            }
                        }else{
                            plugin[values] = options[values]
                        }
                    }
                }
                plugin.clickElement.hide();
                //If we can't scroll
                if (plugin.last !== 'string' && plugin.elt.height() + plugin.elt.offset().top < $w.height()) {
                    plugin.ajaxMethod(plugin);
                }
                $(document).on(plugin.plugInst, function() {
                    if (plugin.max && i <= plugin.max && flag) {
                        if ((plugin.elt.height() + plugin.elt.offset().top - 100) >= document.body.scrollTop) {
                            plugin.ajaxMethod(plugin)
                        }
                    }
                    if (flag && i > plugin.max) plugin.clickElement.show();
                });
                plugin.clickElement.on(plugin.clickTapEvent, function() {
                    if (i <= plugin.last && flag) {
                        plugin.ajaxMethod(plugin)
                    }
                });
            };
            // launch the InfiniteScroll
            this.create(this)
        };
        /**
         * The plugin prototype that list all availables methods.
         * @type {Object}
         */
        $[pluginName].prototype = {
            /*
             * @return {object} - The data geted by ajaxMethod
             */
            getData: function(type) {
                return this.response;
            },
            getInstance: function(){
                return this;
            },
            /**
             * Destroy the plugin by stopping any load content
             */
            destroy: function() {
                // destroy stuff
                this.clickElement.hide();
                //this.max = this.last = 0;
                $(document).off(this.plugInst)
                return this;
            }
        };

        return $[pluginName];

    }(window));

    /**
     * The plugin component
     * @param  {object} options - list of all parameters for the jQuery/Zepto module
     * @return {object} - The jQuery/Zepto DOM element
     */
    $.fn[pluginName] = function(options) {
        return $(this).each(function(){
            //var opts = (typeof options !== 'string') ? $.extend({}, defaults, options) : options;
            if (!$(this).data(pluginName)) {
                $(this).data(pluginName, new $[pluginName]($(this), options));
            } else {
                var $plugin = $(this).data(pluginName);
                if (typeof options === 'string') {
                    !$plugin.hasOwnProperty(options) && typeof $plugin[options] === 'function' && $plugin[options].apply($plugin);
                } else {
                    $plugin.create($plugin,options);
                }
            }
        });
    };

}(Zepto || jQuery));
