# Infinite Scroll #

A simple zepto/jquery plugin who allow you to scroll and load as many pages as you want.

Just use the following synthax :

```
$('.content').infiniteScroll({ajaxUrl:'url/to/page', max : 6}) // will load 6 time

//$('.content') is where you want to add your infinite scroll
```
List of options to pass to infiniteScroll function :

```
max : int,                 | set how many time the scroll infinite will automaticaly load content
last : int,                | set how many pages max you want to load. If not set, last = max
ajaxUrl : string,          | the url of the page you want to load to
idButtonPlus : string,     | the id of the button to load more pages once the auto-loaded pages stop
```

To get the datas and add into the web page, use this :
```
var getData = $('.content').infiniteScroll('getData'); // Object with the response of ajax method

```